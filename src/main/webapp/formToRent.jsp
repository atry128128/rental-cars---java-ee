<%@ page import="com.cars.Car" %>
<%@ page import="com.user.User" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: Joanna Kwaśniewska
  Date: 09.12.2019
  Time: 13:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

    <title>Form to rent</title>
    <style>

        .back{
            border: black solid 3px;
            background-color: black;
            color:crimson;
            padding:5px;
            border-radius: 10px;
            position: fixed;
            left: 750px;
            bottom: 72px;
            width: 40px;
            text-align:center;
        }
    </style>
</head>
<body>

<%@ include file="layout.jsp" %><br><br><br><b>
<div class="text"><br><br><br><br>
    <div style="font-size: large"><b>About car:</b></div><br>
   <% Car car = (Car) request.getAttribute("car");
       List<User> users = (List<User>) request.getAttribute("users");
       %>
    Id: <%=car.getId()%><br>
    Brand: <%=car.getBrand()%><br>
    Model: <%=car.getModel()%><br>
    Year: <%=car.getYear()%><br>
    Car color: <%=car.getCarColor()%><br>
    Power: <%=car.getPower()%><br>
    Price per day: <%=car.getPricePerDay()%><br><br>


    <div style="font-size: large"><b>Rent to:</b></div>

<form method="get" action="deleteCarRent">

    <input list="users" name="users">
    <datalist id="users">
<%
        for(User user: users){%>
        <option value="<%=user.getLogin()%>">
        <%}%>
    </datalist><br><br><br>

    <input value="<%=car.getId()%>" type="hidden" name="id">

    <input type="submit" value="Rent !"/>

</form>
</div>
</b>
<div class="back">

    <a onclick="history.back()">Back</a>

</div>
</body>
</html>
