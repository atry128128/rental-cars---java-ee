package com.user.exception;

public class LoginIsTakenException extends Exception{
    public LoginIsTakenException(String message) {
        super(message);
    }
}
