<%@ page import="com.user.User" %>

<%@ page import="java.util.List" %>


<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Table users</title>
    <style>
        body {
            overflow: scroll;
        }

        table {
            border: 2px solid black;
            border-collapse: collapse;
            width: 100%;
            text-align: center;
            font-weight: 400;

            padding: 5px;
            margin-bottom: 100px;


        }

        table td {
            border: 2px solid black;
            border-collapse: collapse;
            text-align: center;
            padding: 5px;

        }
        .back{
            border: black solid 3px;
            background-color: black;
            color:crimson;
            padding:5px;
            border-radius: 10px;
            position: fixed;
            left: 750px;
            bottom: 72px;
            width: 40px;
            text-align:center;
        }
    </style>
</head>
<body>
<%@ include file="layout.jsp" %>

<div class="text">
    <h2>All users</h2>
    <table id="tabela">
        <tr style="color: firebrick ;font-weight: bolder;font-size:20px">
            <td>Id</td>
            <td>Name</td>
            <td>Surname</td>
            <td>Login</td>
            <td>Role</td>
            <td>Email</td>
        </tr>
        <%
            List<User> allUsers =(List<User>) request.getAttribute("allUsers");


            for (User user : allUsers) {
        %>
        <tr>
            <td><%=user.getId()%>
            </td>
            <td><%=user.getName()%>
            </td>
            <td><%=user.getSurname()%>
            </td>
            <td><%=user.getLogin()%>
            </td>
            <td><%=user.getRole()%>
            </td>
            <td><%=user.getEmail()%>
            </td>
            <td><a href="deleteUser?id=<%=user.getId()%>">Delete</a></td>
            </td>

        </tr>
        <%
            }
        %>
    </table>
</div>
</div>
<div class="back">

    <a onclick="history.back()">Back</a>

</div>
</body>
</html>
