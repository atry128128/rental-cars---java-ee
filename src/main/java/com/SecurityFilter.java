package com;

import javax.ejb.Stateless;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
@Stateless
@WebFilter("/*")
public class SecurityFilter implements Filter {


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse resp = (HttpServletResponse) servletResponse;
        HttpSession session = req.getSession(false);

        String isLogged = null;
        if(session!=null){
            isLogged = (String) session.getAttribute("isLogged");
        }

        String url = req.getRequestURI();

        if (!url.contains("tableEditCars")
        ||!url.contains("table-cars-for-edit")
        ||!url.contains("deleteCarRent")
                |!url.contains("table-users")

                || ("true").equals(isLogged)) {

            filterChain.doFilter(req, resp);
        }else {
            resp.sendRedirect(resp.encodeRedirectURL("login.jsp" ));
        }
    }

    @Override
    public void destroy() {
    }
}
