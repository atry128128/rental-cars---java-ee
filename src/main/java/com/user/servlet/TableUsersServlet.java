package com.user.servlet;

import com.user.User;
import com.user.UserService;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/tableUsers")
public class TableUsersServlet extends HttpServlet {
    @EJB
    private UserService userService;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<User> allUsers = userService.getAllUsers();
        req.setAttribute("allUsers", allUsers);
       req.getRequestDispatcher("/table-users.jsp").forward(req, resp);


    }
}
