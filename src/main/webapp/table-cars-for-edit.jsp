<%@ page import="com.cars.Car" %>
<%@ page import="java.util.List" %>


<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Table car for edit</title>
    <style>
        body {
            overflow: scroll;
        }

        table {
            border: 2px solid black;
            border-collapse: collapse;
            width: 100%;
            text-align: center;
            font-weight: 400;
            padding: 5px;

            margin-bottom: 100px;


        }

        table td {
            border: 2px solid black;
            border-collapse: collapse;
            text-align: center;
            padding: 5px;

        }

        .back {
            border: black solid 3px;
            background-color: black;
            color: crimson;
            padding: 5px;
            border-radius: 10px;
            position: fixed;
            left: 750px;
            bottom: 72px;
            width: 40px;
            text-align: center;
        }
    </style>
</head>
<body>
<%@ include file="layout.jsp" %>
<div class="text">
    <h1 style="text-align:center;">Our cars for rent </h1>

    <table summary="tabela" id="tabela">

        <tr style="color: firebrick ;font-weight: bolder;font-size:20px">
            <td>Id</td>
            <td>Brand</td>
            <td>Model</td>
            <td>Year of production</td>
            <td>Car Color</td>
            <td>Power [KM]</td>
            <td>Price for day [EURO]</td>
        </tr>
        <%
            //List<Car> cars = CarService.getAllCars();
            List<Car> cars = (List<Car>) request.getAttribute("cars");

            for (int i = 0; i < cars.size(); i++) {

        %>

        <tr>

            <td><%=cars.get(i).getId()%>
            </td>
            <td><%=cars.get(i).getBrand()%>
            </td>
            <td><%=cars.get(i).getModel()%>
            </td>
            <td><%=cars.get(i).getYear()%>
            </td>
            <td><%=cars.get(i).getCarColor()%>
            </td>
            <td><%=cars.get(i).getPower()%>
            </td>
            <b>
                <td><%=cars.get(i).getPricePerDay()%>
                </td>
            </b>

            <td>

                <form method="get" action="changePrice">

                    <input name="id" type="hidden" value="<%=cars.get(i).getId()%>">
                    <input name="pricePerDay" type="number">

                    <input type="submit" value="Change price"/>

                </form>
                <a href="changePrice?id=<%=cars.get(i).getId()%>&pricePerDay=<%=request.getParameter("pricePerDay")%>"></a>
            </td>

            <td><a href="deleteCar?id=<%=cars.get(i).getId()%>">Delete</a></td>

            <td><a href="rentCar?id=<%=cars.get(i).getId()%>">Rent</a></td>


                <%
    }%>

                <%
            if (request.getAttribute("isDeleted") !=null ){%>
        <tr>
            <th colspan="10" style="background-color: crimson">Rental car and removed from the list of available cars
            </th>
        </tr>
        <%}%>

        <tr>
            <th colspan="10"><br><a style="color: crimson; font-size: 17px" href="formToAddCar.jsp"><b>Add new
                car</b></a><br><br></th>
        </tr>


    </table>
</div>
<div class="back">

    <a onclick="history.back()">Back</a>

</div>
</body>
</html>
