<%--
  Created by IntelliJ IDEA.
  User: Joanna Kwaśniewska
  Date: 30.11.2019
  Time: 22:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Form</title>
    <style>

        .back {
            border: black solid 3px;
            background-color: black;
            color: crimson;
            padding: 5px;
            border-radius: 10px;
            position: fixed;
            left: 1100px;
            bottom: 80px;
            width: 40px;
            text-align: center;
        }
    </style>
</head>
<body>
<%@ include file="layout.jsp" %>
<br><br><br><div class="text"><br><br>
<b><div style="font-size: 15px"> Feel free to write to us. Please fill in the form and get in touch with us regarding your car rental query.</div><br></b>
<form  style="margin-left:-20px" method="get" action="form"><b>
    Name:
    <input name="name" type="text">
    <br/><br/>
    Surname:
    <input name="surname" type="text">
    <br/><br/>
    Phone number:
    <input name="phoneNumber" type="number">
    <br/><br/>
    E-mail:
    <input name="email" type="email">
    <br/><br/>
    Subject:
    <input list="subject" name="subject">
    <datalist id="subject">
        <option value="rent quote">
        <option value="free dates">
        <option value="information about cars">
        <option value="individual valuation">
        <option value="long term rental">
        <option value="other">

    </datalist>
    <br/><br/>

    <textarea name="text" rows="10" cols="30">
        Message
    </textarea><br><br/>
    <input type="submit" value="Send to us"/></b>
</form></div>




<div class="back">
    <a onclick="history.back()">Back</a>
</div>
</body>
</html>
