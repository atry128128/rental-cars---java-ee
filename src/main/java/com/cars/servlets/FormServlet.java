package com.cars.servlets;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/form")
public class FormServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        String phoneNumber = req.getParameter("phoneNumber");
        String email = req.getParameter("email");
        String subject = req.getParameter("subject");
        String text = req.getParameter("text");

        if (name != null
                && phoneNumber != null
                && email != null
                && subject != null
                && text != null) {

            RequestDispatcher requestDispatcher = req.getRequestDispatcher("/formSubmitted.jsp");
            requestDispatcher.forward(req, resp);

        }


    }
}
