package com.cars.servlets;

import com.cars.Car;
import com.cars.CarColor;
import com.cars.CarService;
import com.cars.exception.CarNotFoundException;
import com.user.UserService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;



@WebServlet("/cars")

public class CarsServlet extends HttpServlet {
    @EJB
    private CarService carService;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.setContentType("text/html;charset=UTF-8");
        List<Car> cars = carService.getAllCars();
        resp.getWriter().write(cars.toString());

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String brand = req.getParameter("brand");
        String model = req.getParameter("model");
        Integer year = Integer.parseInt(req.getParameter("year"));

        CarColor carColor= CarColor.valueOf( req.getParameter("carColor"));
        Integer power = Integer.parseInt(req.getParameter("power"));
        double pricePerDay = Double.parseDouble(req.getParameter("pricePerDay"));

        int id = 0;

        Car newCar = new Car(id++, brand,model,year,carColor,power,pricePerDay);

        carService.addNewCar(newCar);
        resp.getWriter().write(brand+" "+model+" has been added!");
    }


    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = Integer.parseInt(req.getParameter("id"));
        double pricePerDay = Double.parseDouble(req.getParameter("email"));


        try {
            carService.updatePrice(id, pricePerDay);
        } catch (CarNotFoundException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = Integer.parseInt(req.getParameter("id"));
        carService.deleteCarById(id);

    }

}

