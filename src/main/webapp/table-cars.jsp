<%@ page import="com.cars.Car" %>
<%@ page import="java.util.List" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Table Cars</title>
    <style>
        body {
            overflow: scroll;
        }

        table {
            border: 2px solid black;
            border-collapse: collapse;
            width: 100%;
            text-align: center;
            font-weight: 400;

            padding: 5px;
            margin-bottom: 100px;


        }

        table td {
            border: 2px solid black;
            border-collapse: collapse;
            text-align: center;
            padding: 5px;

        }
        .back{
            border: black solid 3px;
            background-color: black;
            color:crimson;
            padding:5px;
            border-radius: 10px;
            position: fixed;
            left: 750px;
            bottom: 72px;
            width: 40px;
            text-align:center;
        }
    </style>
</head>
<body>
<%@ include file="layout.jsp" %>
<div class="text">
    <h1 style="text-align:center;">Our cars for rent </h1>

    <table id="tabela" style="overflow:scroll;">

        <tr style="color: firebrick ;font-weight: bolder;font-size:20px">
            <td>Id</td>
            <td>Brand</td>
            <td>Model</td>
            <td>Year of production</td>
            <td>Car Color</td>
            <td>Power [KM]</td>
            <td>Price for day [EURO]</td>
        </tr>
        <%
            // List<Car> cars = CarService.getAllCars();
            List<Car> cars = (List<Car>) request.getAttribute("cars");
            for (int i = 0; i < cars.size(); i++) {

        %>

        <tr>

            <td><%=cars.get(i).getId()%>
            </td>
            <td><%=cars.get(i).getBrand()%>
            </td>
            <td><%=cars.get(i).getModel()%>
            </td>
            <td><%=cars.get(i).getYear()%>
            </td>
            <td><%=cars.get(i).getCarColor()%>
            </td>
            <td><%=cars.get(i).getPower()%>
            </td>

            <td>
                <b>
                    <%=cars.get(i).getPricePerDay()%>
                </b>
            </td>
        </tr>

        <%
            }%>


    </table>
</div>

<div class="back">

    <a onclick="history.back()">Back</a>

</div>
</body>
</html>

