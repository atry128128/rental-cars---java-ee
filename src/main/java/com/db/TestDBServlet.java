package com.db;

import com.db.DbConnector;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@WebServlet("/testDB")
public class TestDBServlet extends HttpServlet {
@EJB
private DbConnector dbConnector;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws  ServletException, IOException {

        try {
            dbConnector.createConnection();
            resp.getWriter().write("udalo sie polaczyc");
        } catch (SQLException e) {
            e.printStackTrace();
            resp.getWriter().write("nie udalo sie polaczyc");
        }
    }

}
