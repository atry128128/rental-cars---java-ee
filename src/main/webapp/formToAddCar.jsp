<%@ page import="com.cars.CarColor" %><%--
  Created by IntelliJ IDEA.
  User: Joanna Kwaśniewska
  Date: 07.12.2019
  Time: 14:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Form to add</title>
    <style>
        .back{
            border: black solid 3px;
            background-color: black;
            color:crimson;
            padding:5px;
            border-radius: 10px;
            position: fixed;
            left: 750px;
            bottom: 72px;
            width: 40px;
            text-align:center;
        }
    </style>
</head>
<body>
<%@ include file="layout.jsp" %><br><br><br>
<div class="text">

    <form  method="post" action="addCar">
        <h2>Add new car</h2><br>
        Brand:
        <input name="brand" type="text">
        <br/><br>
        Model:
        <input name="model" type="text">
        <br/><br>
        Year:
        <input name="year" type="number">
        <br/><br>
        Car color:
        <input list="carColor" name="carColor">
        <datalist id="carColor">
            <option value="<%=CarColor.BLACK%>">
            <option value="<%=CarColor.RED%>">
            <option value="<%=CarColor.GREEN%>">
            <option value="<%=CarColor.YELLOW%>">
            <option value="<%=CarColor.BLUE%>">
            <option value="<%=CarColor.WHITE%>">
        </datalist>
        <br/><br>
        Power:
        <input name="power" type="number">
        <br/><br>
        Price per day:
        <input name="pricePerDay" type="number">
        <br/><br>
        <input type="submit" value="Add"/>
    </form></div>

</div>
<div class="back">

    <a onclick="history.back()">Back</a>


</div>
</body>
</html>
