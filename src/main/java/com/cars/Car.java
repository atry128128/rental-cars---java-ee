package com.cars;

public class Car {
    private Integer id;
    private String brand;
    private String model;
    private Integer year;
    private CarColor carColor;
    private Integer power;
    private double pricePerDay;

    public Car(Integer id, String brand, String model, Integer year, CarColor color, Integer power, double pricePerDay) {
        this.id = id;
        this.brand = brand;
        this.model = model;
        this.year = year;
        this.carColor = color;
        this.power = power;
        this.pricePerDay = pricePerDay;
    }
    public Car(int id, double pricePerDay){
        this.id = id;
        this.pricePerDay = pricePerDay;
    }

    public Car(String brand, String model, Integer year, CarColor carColor, Integer power, double pricePerDay) {
        this.brand = brand;
        this.model = model;
        this.year = year;
        this.carColor = carColor;
        this.power = power;
        this.pricePerDay = pricePerDay;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public CarColor getCarColor() {
        return carColor;
    }

    public void setCarColor(CarColor carColor) {
        this.carColor = carColor;
    }

    public Integer getPower() {
        return power;
    }

    public void setPower(Integer power) {
        this.power = power;
    }

    public double getPricePerDay() {
        return pricePerDay;
    }

    public void setPricePerDay(double pricePerDay) {
        this.pricePerDay = pricePerDay;
    }

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", year=" + year +
                ", carColor=" + carColor +
                ", power=" + power +
                ", pricePerDay=" + pricePerDay +
                '}';
    }
}
