<%--
  Created by IntelliJ IDEA.
  User: Joanna Kwaśniewska
  Date: 28.11.2019
  Time: 13:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Main Page</title>
    <link href="https://fonts.googleapis.com/css?family=GFS+Didot&display=swap" rel="stylesheet">
    <style>
        @font-face {
            font-family: 'GFS Didot';
            font-style: normal;
            font-weight: 400;
            font-display: swap;
            src: local('GFS Didot Regular'), local('GFSDidot-Regular'), url(https://fonts.gstatic.com/s/gfsdidot/v9/Jqzh5TybZ9vZMWFssvwSE-3HJSA.woff2) format('woff2');
            unicode-range: U+0370-03FF;
        }

        #redCarpet {
            color: deeppink;
        }

        .text {
            font-family: 'GFS Didot', 'Comic Sans MS', serif;
        }
    </style>
</head>
<body>
<%@ include file="layout.jsp" %>

<div class="text">
    <h2><br><b>Sports cars for rent</b></h2><br>
    <img src="https://i.pinimg.com/originals/82/69/1f/82691f8cbdfb106abb1b35944cf4b9bd.jpg"
         align="left" alt="audiMainPage" width="400px" height="400px">
    <div style="padding: 30px; font-size: 20px; font-family: 'GFS Didot',Didot,Didot LT STD,serif; ">Bad Cars is one of the biggest online car hire service providers in the United States of America, with 1M+ happy
        customers.
        A car rental in the USA is your freedom-pass to travel like a local and cover as much road as you want to,
        without any hassle. And, at USA Cars Rental, we make sure that you get hold of your perfect deal in just a few
        clicks!
        We are a car rental broker company that brings pre-negotiated deals to you, from leading suppliers at heavily
        discounted rates. Understanding customers and their car rental needs is our major priority, and we leave no
        stone unturned to help them find the right car for their vacation or business trip!
    </div>


    </div>

</div>



</body>
</html>
