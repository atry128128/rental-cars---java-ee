package com.cars;

import com.cars.dao.CarDAO;
import com.cars.exception.CarNotFoundException;


import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.List;
@Stateless
public class CarService {
    @EJB
    private CarDAO carDAO;


    public  List<Car> getAllCars() {

        return carDAO.getAllUser();
    }

    public  Car findCarById(int id) throws CarNotFoundException {
        Car car = carDAO.selectById(id);
        if (car == null) {

             throw new CarNotFoundException("Car with id: " + id + " not found.");
        } else {
            return car;

        }

    }


    public  void addNewCar(Car newCar) {
        carDAO.saveNewCar(newCar);

    }

    public  Car updatePrice(int id, double pricePerDay) throws CarNotFoundException {
        Car car = carDAO.updatePrice(id, pricePerDay);

        if (id == car.getId()) {
            return car;

        } else throw new CarNotFoundException("Car with id" + id + "not found.");

    }

    public  void deleteCarById(int id) {

        carDAO.deleteCarById(id);
    }
}
