<%--
  Created by IntelliJ IDEA.
  User: Joanna Kwaśniewska
  Date: 03.12.2019
  Time: 15:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Register</title>
    <style>
        .back{
            border: black solid 3px;
            background-color: black;
            color:crimson;
            padding:5px;
            border-radius: 10px;
            position: fixed;
            left: 1100px;
            bottom: 72px;
            width: 40px;
            text-align:center;
        }

    </style>
</head>
<body>
<%@ include file="layout.jsp" %><br><br><br><div class="text">
<form  method="post" action="users">
    <h2>Register now</h2><br>
    Name:
    <input name="name" type="text">
    <br/><br>
    Surname:
    <input name="surname" type="text">
    <br/><br>
    Login:
    <input name="login" type="text">
    <br/><br>
    Password:
    <input name="password" type="password">
    <br/><br>
    Role:
    <input list="role" name="role">
    <datalist id="role">
        <option value="admin">
        <option value="user">
    </datalist>
    <br/><br>
    Email:
    <input name="email" type="email">
    <br/><br>
    <input type="submit" value="Register"/>
</form></div>
<div class="back">

    <a onclick="history.back()">Back</a>

</div>
</body>
</html>
