package com.cars.servlets;

import com.cars.Car;
import com.cars.CarColor;
import com.cars.CarService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/addCar")
public class AddNewCarServlet extends HttpServlet {
    @EJB
    private CarService carService;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String brand = req.getParameter("brand");
        String model = req.getParameter("model");
        Integer year = Integer.parseInt(req.getParameter("year"));

        CarColor carColor= CarColor.valueOf( req.getParameter("carColor"));
        Integer power = Integer.parseInt(req.getParameter("power"));
        double pricePerDay = Double.parseDouble(req.getParameter("pricePerDay"));

        List<Car> cars = carService.getAllCars();
        req.setAttribute("cars", cars);

        carService.addNewCar(new Car(brand,model,year,carColor,power,pricePerDay));
        resp.sendRedirect(resp.encodeRedirectURL("tableEditCars"));


    }
}
