package com.cars.dao;

import com.cars.Car;
import com.cars.CarColor;
import com.db.DbConnector;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
@Stateless
public class CarDAO {
    @EJB
    private DbConnector dbConnector;
    public  List<Car> getAllUser() {
        List<Car> cars = new ArrayList<>();
        try {
            Connection connection = dbConnector.createConnection();
            PreparedStatement ps = connection.prepareStatement("select * from car;");
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                int id = rs.getInt("id");
                String brand = rs.getString("brand");
                String model = rs.getString("model");
                Integer year = rs.getInt("year");
                CarColor carColor = CarColor.valueOf(rs.getString("car_color"));
                Integer power = rs.getInt("power");
                Double pricePerDay = rs.getDouble("price_per_day");

                Car carFromRs = new Car(id, brand, model, year, carColor, power, pricePerDay);
                cars.add(carFromRs);

            }
            ps.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cars;
    }

    public  void saveNewCar(Car newCar) {
        try {
            Connection connection = dbConnector.createConnection();
            PreparedStatement ps = connection.prepareStatement("insert into car " +
                    "values (null, ?, ?, ?, ?, ?,?);");

            ps.setString(1, newCar.getBrand());
            ps.setString(2, newCar.getModel());
            ps.setInt(3, newCar.getYear());
            ps.setString(4, newCar.getCarColor().toString());
            ps.setInt(5, newCar.getPower());
            ps.setDouble(6, newCar.getPricePerDay());

            ps.executeUpdate();// -->>insert, update, delete
            //  ps.executeQuery(); -->> select
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public  void deleteCarById(int id) {
        try {
            Connection connection = dbConnector.createConnection();
            PreparedStatement ps = connection.prepareStatement("DELETE FROM car\n" +
                    "WHERE id = ?; ");
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public  Car selectById(int id) {
        Car car = null;

        try {
            Connection connection = dbConnector.createConnection();
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM car WHERE id = ?;");


            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();
            if(rs.next()) {

                String brand = rs.getString("brand");
                String model = rs.getString("model");
                int year = rs.getInt("year");
                CarColor carColor = CarColor.valueOf(rs.getString("car_color"));
                int power = rs.getInt("power");
                Double pricePerDay = rs.getDouble("price_per_day");

                car = new Car(id, brand, model, year, carColor, power, pricePerDay);
            }
            //ps.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return car;
    }

    public  Car updatePrice(int id, double pricePerDay){
        Car carToUpdate = null;
        try{
            Connection connection = dbConnector.createConnection();

            PreparedStatement ps = connection.prepareStatement("UPDATE car SET price_per_day=? WHERE ID=?;");
            ps.setDouble(1, pricePerDay);
            ps.setInt(2, id);

            ps.executeUpdate();
            carToUpdate = new Car(id, pricePerDay);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return carToUpdate;
}

}

