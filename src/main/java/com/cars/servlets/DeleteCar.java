package com.cars.servlets;

import com.cars.Car;
import com.cars.CarService;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Stateless
@WebServlet("/deleteCar")
public class DeleteCar extends HttpServlet {
    @EJB
    private CarService carService;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Integer id = Integer.parseInt(req.getParameter("id"));

        List<Car> cars = carService.getAllCars();
        req.setAttribute("cars", cars);
        carService.deleteCarById(id);
        req.getRequestDispatcher("/tableEditCars").forward(req, resp);

    }
}
