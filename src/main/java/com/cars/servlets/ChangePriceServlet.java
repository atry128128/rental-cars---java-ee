package com.cars.servlets;

import com.cars.CarService;
import com.cars.exception.CarNotFoundException;


import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/changePrice")
public class ChangePriceServlet extends HttpServlet {
    @EJB
    private CarService carService;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws  IOException {
        String price = req.getParameter("pricePerDay");
        String id = req.getParameter("id");

        try {
            int idAsInt = Integer.parseInt(id);
            double priceAsDouble = Double.parseDouble(price);
            carService.updatePrice(idAsInt, priceAsDouble);
        } catch (CarNotFoundException e) {
            e.printStackTrace();
        }

        resp.sendRedirect(resp.encodeRedirectURL("tableEditCars"));
    }
}
