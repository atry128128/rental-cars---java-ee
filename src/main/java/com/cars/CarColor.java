package com.cars;

public enum CarColor {

    GREEN,
    YELLOW,
    BLACK,
    BLUE,
    RED,
    WHITE

}
