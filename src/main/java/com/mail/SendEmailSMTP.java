package com.mail;


import com.sun.mail.smtp.SMTPTransport;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Date;
import java.util.Properties;

public class SendEmailSMTP {


    private static final String SMTP_SERVER = "smtp.gmail.com";
    private static final String USERNAME = "mailtotest123321";
    private static final String PASSWORD = "";

    private static final String EMAIL_FROM = "haslodomailadotestu@gmail.com";
    private static final String EMAIL_TO = "atry128128@gmail.com";
    private static final String EMAIL_TO_CC = "";

    private static final String EMAIL_SUBJECT = "Test Send Email via SMTP";
    private static final String EMAIL_TEXT = "Hello Java Mail \n ABC123";

    public static void main(String[] args) {


        Properties prop = System.getProperties();
        prop.put("mail.smtp.host", "smtp.gmail.com");

        prop.put("mail.smtp.ssl.trust", "smtp.gmail.com");
        prop.put("mail.smtp.port", "587");
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.starttls.enable", "true");


        Session session = Session.getInstance(prop, null);
        Message msg = new MimeMessage(session);


        try {


            msg.setFrom(new InternetAddress(EMAIL_FROM));


            msg.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(EMAIL_TO, false));


            msg.setRecipients(Message.RecipientType.CC,
                    InternetAddress.parse(EMAIL_TO_CC, false));


            msg.setSubject(EMAIL_SUBJECT);


            msg.setText(EMAIL_TEXT);

            msg.setSentDate(new Date());


            SMTPTransport t = (SMTPTransport) session.getTransport("smtp");


            t.connect(SMTP_SERVER, USERNAME, PASSWORD);


            t.sendMessage(msg, msg.getAllRecipients());

            System.out.println("Response: " + t.getLastServerResponse());

            t.close();

        } catch (MessagingException e) {
            e.printStackTrace();
        }


    }
}
