package com.user.servlet;

import com.user.User;
import com.user.exception.LoginIsTakenException;
import com.user.exception.UserNotFoundException;
import com.user.UserService;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/users")
public class UserServlet extends HttpServlet {
    @EJB
    private UserService userService;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=UTF-8");

        List<User> allUsers = userService.getAllUsers();


        resp.getWriter().write(allUsers.toString());

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        String surname = req.getParameter("surname");
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        String role = req.getParameter("role");
        String email = req.getParameter("email");

        List<User> users = userService.getAllUsers();

        int id = 0;

        for (User user : users) {
            if (login.equals(user.getLogin())) {
                resp.getWriter().write("This login is already taken. Choose another one");
                return;
            }
        }

        User newUser = new User(id++, name, surname, login, password, role, email);
        try {
            userService.addNewUser(newUser);
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("registrationSuccessful.jsp");
            requestDispatcher.forward(req, resp);


        } catch (LoginIsTakenException loginIsTaken) {
            loginIsTaken.printStackTrace();
        }

    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = Integer.parseInt(req.getParameter("id"));
        String name = req.getParameter("name");
        String surname = req.getParameter("surname");
        String password = req.getParameter("password");

        String email = req.getParameter("email");

        try {
            userService.updateUser(id, name, surname, password, email);
        } catch (UserNotFoundException e) {
            e.printStackTrace();
        }
    }
    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = Integer.parseInt(req.getParameter("id"));

        userService.deleteUserById(id);
    }
}
