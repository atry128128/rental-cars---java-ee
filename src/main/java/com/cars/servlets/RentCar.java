package com.cars.servlets;

import com.cars.Car;
import com.cars.CarService;
import com.cars.exception.CarNotFoundException;
import com.user.User;
import com.user.UserService;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/rentCar")
public class RentCar extends HttpServlet {
    @EJB
    private CarService carService;
    @EJB
    private UserService userService;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String id = req.getParameter("id");


        try {
            int idAsInt = Integer.parseInt(id);
            Car car = carService.findCarById(idAsInt);
            req.setAttribute("car", car);
            List<User>users = userService.getAllUsers();
            req.setAttribute("users", users);
            req.getRequestDispatcher("/formToRent.jsp").forward(req, resp);

        } catch (CarNotFoundException e) {
            e.printStackTrace();
        }



    }
}
