package com.rental;

import com.cars.Car;
import com.user.User;

import java.util.Date;

public class Rental {
   private Integer id;
    private User user;
    private Car car;
    private Date rentedAt;
    private Date returnAt;
   private  Date expectedReturnDate;
    private double price;
    private  String city;

    public Rental(Integer id, User user, Car car, Date rentedAt, Date returnAt, Date expectedReturnDate, double price, String city) {
        this.id = id;
        this.user = user;
        this.car = car;
        this.rentedAt = rentedAt;
        this.returnAt = returnAt;
        this.expectedReturnDate = expectedReturnDate;
        this.price = price;
        this.city = city;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Date getRentedAt() {
        return rentedAt;
    }

    public void setRentedAt(Date rentedAt) {
        this.rentedAt = rentedAt;
    }

    public Date getReturnAt() {
        return returnAt;
    }

    public void setReturnAt(Date returnAt) {
        this.returnAt = returnAt;
    }

    public Date getExpectedReturnDate() {
        return expectedReturnDate;
    }

    public void setExpectedReturnDate(Date expectedReturnDate) {
        this.expectedReturnDate = expectedReturnDate;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "Rental{" +
                "id=" + id +
                ", user=" + user +
                ", car=" + car +
                ", rentedAt=" + rentedAt +
                ", returnAt=" + returnAt +
                ", expectedReturnDate=" + expectedReturnDate +
                ", price=" + price +
                ", city='" + city + '\'' +
                '}';
    }
}
