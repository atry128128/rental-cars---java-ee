package com.cars.servlets;

import com.cars.Car;
import com.cars.CarService;
import com.cars.exception.CarNotFoundException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/deleteCarRent")
public class DeleteCarRent extends HttpServlet {
    @EJB
    private CarService carService;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String id = req.getParameter("id");

        if (id!=null) {
            int idAsInt = Integer.parseInt(id);
            carService.deleteCarById(idAsInt);
            req.setAttribute("isDeleted", "true");
            req.getRequestDispatcher("/tableEditCars").forward(req, resp);

        }
    }
}
