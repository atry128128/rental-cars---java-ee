package com.user;

import com.user.dao.UserDAO;
import com.user.exception.LoginIsTakenException;
import com.user.exception.UserNotFoundException;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;
@Stateless
public class UserService {
    @EJB
    private UserDAO userDAO;

    public  List<User> getAllUsers() {

        return userDAO.getAllUser();
    }

    public  User findUserById(int id) throws UserNotFoundException {
        User user = userDAO.selectById(id);
        if (user.getId() == id) {
            return user;
        } else {
            throw new UserNotFoundException("User with id: " + id + " not found.");
        }
    }

    public  void addNewUser(User newUser) throws LoginIsTakenException {
        List<User> users = getAllUsers();
        for (User user : users) {
            if (user.getLogin().equals(newUser.getLogin())) {
                throw new LoginIsTakenException("Login: " + newUser.getLogin() + " is taken. Choose another.");
            }
        }

        userDAO.saveNewUser(newUser);
    }

    public  User updateUser(int id, String name, String surname, String password, String email) throws UserNotFoundException {

        User user = userDAO.updateUser(id, name, surname, password, email);
        if (user.getId() == id) {
            return user;
        } else {
            throw new UserNotFoundException("User with id: " + id + " not found.");
        }
    }

    public  void deleteUserById(int id) {
        userDAO.deleteUserById(id);
    }
}
