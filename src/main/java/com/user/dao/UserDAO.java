package com.user.dao;


import com.db.DbConnector;
import com.user.User;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class UserDAO {
    @EJB
    private DbConnector dbConnector;

    public List<User> getAllUser() {
        List<User> users = new ArrayList<>();
        try {
            Connection connection = dbConnector.createConnection();
            PreparedStatement ps = connection.prepareStatement("select * from user;");
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String surname = rs.getString("surname");
                String login = rs.getString("login");
                String password = rs.getString("password");
                String role = rs.getString("role");
                String email = rs.getString("email");

                User userFromRs = new User(id, name, surname, login, password, role, email);
                users.add(userFromRs);

            }
            ps.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }

    public void saveNewUser(User newUser) {
        try {
            Connection connection = dbConnector.createConnection();
            PreparedStatement ps = connection.prepareStatement("insert into user " +
                    "values (null, ?, ?, ?, ?, ?,?);");

            ps.setString(1, newUser.getName());
            ps.setString(2, newUser.getSurname());
            ps.setString(3, newUser.getLogin());
            ps.setString(4, newUser.getPassword());
            ps.setString(5, newUser.getRole());
            ps.setString(6, newUser.getEmail());

            ps.executeUpdate();// -->>insert, update, delete
            //  ps.executeQuery(); -->> select
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteUserById(int id) {
        try {
            Connection connection = dbConnector.createConnection();
            PreparedStatement ps = connection.prepareStatement("DELETE FROM user\n" +
                    "WHERE id = ?; ");
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public User selectById(int id) {
        User user = null;
        try {
            Connection connection = dbConnector.createConnection();
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM user WHERE id = ?;");

            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();

            String name = rs.getString("name");
            String surname = rs.getString("surname");
            String login = rs.getString("login");
            String password = rs.getString("password");
            String role = rs.getString("role");
            String email = rs.getString("email");


            user = new User(id, name, surname, login, password, role, email);
            ps.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return user;
    }


    public User updateUser(int id, String name, String surname, String password, String email) {
        User userToUpdate = null;
        try {
            Connection connection = dbConnector.createConnection();
            PreparedStatement ps = connection.prepareStatement("UPDATE user SET name = '?'," +
                    " surname= '?'" +
                    " password= '?'" +
                    " email= '?'" +
                    " WHERE id = ?;");

            ps.setString(1, name);
            ps.setString(2, surname);
            ps.setString(3, password);
            ps.setString(4, email);
            ps.setInt(5, id);
            ps.executeUpdate();
            userToUpdate = new User(id, name, surname, password, email);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userToUpdate;
    }
}
