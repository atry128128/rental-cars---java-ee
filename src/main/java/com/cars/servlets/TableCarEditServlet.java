package com.cars.servlets;

import com.cars.Car;
import com.cars.CarService;
import com.cars.exception.CarNotFoundException;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
@Stateless
@WebServlet("/tableEditCars")
public class TableCarEditServlet extends HttpServlet {
    @EJB
    private CarService carService;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Car> cars = carService.getAllCars();
        req.setAttribute("cars", cars);
        req.getRequestDispatcher("/table-cars-for-edit.jsp").forward(req, resp);
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String id = req.getParameter("id");
        String pricePerDay = req.getParameter("pricePerDay");
        try {
            int idAsInt = Integer.parseInt(id);
            double priceAsDouble = Double.parseDouble(pricePerDay);
            carService.updatePrice(idAsInt, priceAsDouble);

        } catch (CarNotFoundException e) {
            e.printStackTrace();
        }

        resp.sendRedirect(resp.encodeRedirectURL("table-cars-for-edit.jsp"));

    }
}
