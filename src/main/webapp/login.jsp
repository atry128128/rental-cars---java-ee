<%--
  Created by IntelliJ IDEA.
  User: Joanna Kwaśniewska
  Date: 01.12.2019
  Time: 12:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>
    <style>
        .back {
            border: black solid 3px;
            background-color: black;
            color: crimson;
            padding: 5px;
            border-radius: 10px;
            position: fixed;
            left: 750px;
            bottom: 72px;
            width: 40px;
            text-align: center;
        }

        #invalidPassword {
            background-color: crimson;
            width: 250px;
            text-align: center;
            margin-left: 50%;

        }

    </style>
</head>
<body>
<%@ include file="layout.jsp" %>
<br><br><br><br><br><div class="text"><br><br><br>
<b><h2>Sign in</h2><br>
    <form method="post" action="login">
        Login:
        <input name="login" type="text">
        <br/><br>
        Password:
        <input name="password" type="password">
        <br/><br>
        <input type="submit" value="Log in"/>
    </form></b></div>
</b>


<%
    isLogged = (String) session.getAttribute("isLogged");
    String login = request.getParameter("login");
    if (isLogged == null && login != null) {
%>
<div id="invalidPassword"><b>Invalid login or password</b></div>
<%
    }


%>


<div class="back">

    <a onclick="history.back()">Back</a>

</div>
</body>
</html>
