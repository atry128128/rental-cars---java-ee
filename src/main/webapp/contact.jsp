<%--
  Created by IntelliJ IDEA.
  User: Joanna Kwaśniewska
  Date: 30.11.2019
  Time: 19:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Contact</title>
    <style>
        .back{
            border: black solid 3px;
            background-color: black;
            color:crimson;
            padding:5px;
            border-radius: 10px;
            position: fixed;
            left: 750px;
            bottom: 72px;
            width: 40px;
            text-align:center;
        }
        #contact{
            padding-top:75px;
        }
    </style>
</head>
<body>
<%@ include file="layout.jsp" %><br>
<div class="text">
    <div id="contact">
<h1 >Contact:</h1><br>

    Agency location<br>
    Address: <b>2 Boulevard de la Corniche 20000 Casablanca, Casablanca</b><br><br>

    E-mail: <b>badcars@aaarentcars.com</b><br><br>

    Phone: <b>+212 (0)522 43 00 1</b><br><br>

    Fax: <b>+212 (0)522 43 06 19</b><br><br>
    </div>

<div class="back">

    <a onclick="history.back()">Back</a>

</div>

</div>
</body>
</html>
