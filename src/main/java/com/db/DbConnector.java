package com.db;

import javax.ejb.Stateless;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
@Stateless
public class DbConnector {

    public static final String JDBC_URL = "jdbc:mysql://localhost:3306/cars_rental?autoReconnect=true&useSSL=false&serverTimezone=UTC";

    public static final String JDBC_USER = "root";

    public static final String JDBC_PASSWORD = "root";

    public  Connection createConnection() throws SQLException {

        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
            return DriverManager.getConnection(JDBC_URL, JDBC_USER, JDBC_PASSWORD);

    }
}
