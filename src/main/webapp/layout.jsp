<%--
  Created by IntelliJ IDEA.
  User: Joanna Kwaśniewska
  Date: 30.11.2019
  Time: 16:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>layout</title>

    <style>
        body {
            background-color: grey;
            text-align: center;
            top: 0;
        }

        .header {
            font-size: 23px;
            color: crimson;
            text-align: center;
            background-color: black;
            position: relative;
            top: 0;
            margin: -8px;
            height: auto;
            width: calc(100% - 10px);
            padding: 10px;
        }

        .menu {
            position: absolute;
            color: black;
            left: 0;
            width: 14%;
            text-align: right;
            top: 100px;
            font-size: large;
            height: 200px;
            border-right: 3px solid black;
            padding-top: 100px;
            padding-right: 20px;
        }

        .text {

            float: right;
            right: 0;
            width: 80%;
            color: black;
            margin-top:-350px;
            height:500px;
            padding-bottom: 200px;
        }

        .footer {
            font-size: 13px;
            color: crimson;
            text-align: center;
            background-color: black;
            position: fixed;
            bottom: 8px;
            margin: -8px;
            height: auto;
            width: calc(100% - 10px);
            padding: 10px;
            z-index: 2;
            font-family: 'High Tower Text';
        }


    </style>
</head>
<body>

<div class="header">
    <h1 style="font-family:'High Tower Text'">Bad cars</h1>
    <h2 style="font-family:'High Tower Text'; margin-left: 500px; margin-top: -40px">rental sport cars</h2>
</div>
<div class="text"><br><br><br>


</div>
<div class="footer">
    <b>
        <div style="font-size: 10px">Bad cars.</div>
        Follow us on
        <a href="https://www.instagram.com"><img
                src="https://rootblog.pl/wp-content/uploads/2019/10/instagram-dark-mode.png"
                style="vertical-align: middle" alt="insta" width="40px" height="24px"></a>
    </b>
</div>

<div class="menu"><b>
    <a style="color: black" href="mainPage.jsp">Main Page</a><br>
    <%
       String isLogged = (String) session.getAttribute("isLogged");
        if (isLogged != null) {
    %><a style="color: black" href="tableEditCars">Our sport cars</a><br>
    <%} else {%>
    <a style="color: black" href="tableCars">Our sport cars</a><br>
    <%}%>
    <%
        isLogged = (String) session.getAttribute("isLogged");
        if (isLogged == null) {
    %>
    <a style="color: black" href="contact.jsp">Contact</a><br><%}%>
    <a style="color: black" href="form.jsp">Ask us</a><br>
    <a style="color: black" href="login.jsp">Log in
        <div style="display: inline-block; font-size: 12px">(for moderator)</div></a><br>
    <%
        isLogged = (String) session.getAttribute("isLogged");
        if (isLogged == null) {
    %>
    <a style="color: black" href="register.jsp">Register</a><br><%}%>
    <%
         isLogged = (String) session.getAttribute("isLogged");
        if (isLogged != null) {
    %>
    <a style="color: black" href="tableUsers">Table users</a><br><%}%>

    <br>

</b>
</div>

</body>
</html>
</body>
</html>
